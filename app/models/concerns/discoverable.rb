module Discoverable
 extend ActiveSupport::Concern

 included do
  def self.get_trending(params,normalize_power )
    order("power((("+ params.map {|param| "#{param}"}.join('+')+")/(current_timestamp-#{self.table_name}.created_at)),#{normalize_power}) desc")
  end

  def self.get_most_interacted(params)
   order("("+params.map {|param| "#{param}"}.join(',')+") desc")
  end
 end

end
